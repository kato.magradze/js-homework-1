const A = false;
const B = true;
const C = false; 
const D = undefined;
const E = undefined;
const F = true; 
const H = 10;
const I = -3;
const J = 10; 

//1
console.log(A && (B || !D));

//2
console.log((B && (!A || D)) != !F);

//3
console.log((C || (!B && !F)) && (A || E));

//4
console.log((A && (B === F)) || ((D || B) && !C));

//5
console.log( C || ( ((F === !A) || (E === D)) === (!B || F) ) );

//6
console.log(!C && (J == H));  //როგორც გავიგე, პირობაში სადაც 'უდრის' წერია იქ == უნდა გამოვიყენოთ, ექვივალენტურის შემთხვევაში კი ===.

//7 
console.log((A || !B) || (H > I)); 

//8
console.log(((B != C) || (I > G)) != (!A && B));

//9
console.log( (!A || (J === C)) && (E || (!C === !A)) );

//10
console.log((A || ((H > I) && (J >= H))) != (B || A) );
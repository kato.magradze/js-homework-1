//ნეო
const neoFirstSubject = 60.2;
const neoSecondSubject = 57.3;
const neoThirdSubject = 72.4;
const neoFourthSubject = 88;

//ინდიანა
const indianaFirstSubject = 78.2;
const indianaSecondSubject = 52.3;
const indianaThirdSubject = 66.4;
const indianaFourthSubject = 80;

//სევერუსა
const severusFirstSubject = 75.2;
const severusSecondSubject = 67.3;
const severusThirdSubject = 54.4;
const severusFourthSubject = 90;

//ალადინი
const aladdinFirstSubject = 80.2;
const aladdinSecondSubject = 52.3;
const aladdinThirdSubject = 68.4;
const aladdinFourthSubject = 76;


//consts
const numOfSubjects = 4;
const maxPoints = 100;


//Neo Results
let neoTotalPoints = 0;
let neoTotalAverage = 0;
let neoAvgPercentage = 0;


neoTotalPoints = neoFirstSubject + neoSecondSubject + neoThirdSubject + neoFourthSubject;
neoTotalAverage = neoTotalPoints / numOfSubjects;
neoAvgPercentage = (neoTotalAverage / maxPoints) * 100; //პირობაში კარგად ვერ მივხვდი ზუსტად რას გვთხოვდნენ პროცენტულ მაჩვენებელში და როგორც გავიგე ისე დავწერე. იმედია სწორია.

console.log("%cRESULTS FOR NEO", "color: green; font-size: 19px; font-weight: bold");
console.log("Neo's points total: " + neoTotalPoints);
console.log("Neo's points average: " + neoTotalAverage);
console.log("Neo's average percentage: " + neoAvgPercentage + "%");
console.log("\n");

//Indiana Results 
let indianaTotalPoints = 0;
let indianaTotalAverage = 0;
let indianaAvgPercentage = 0;

indianaTotalPoints = indianaFirstSubject + indianaSecondSubject + indianaThirdSubject + indianaFourthSubject;
indianaTotalAverage = indianaTotalPoints / numOfSubjects;
indianaAvgPercentage = (indianaTotalAverage / maxPoints) * 100;

console.log("%cRESULTS FOR INDIANA", "color: orange; font-size: 19px; font-weight: bold");
console.log("Indiana's points total: " + indianaTotalPoints);
console.log("Indiana's points average: " + indianaTotalAverage);
console.log("Indiana's average percentage: " + indianaAvgPercentage + "%");
console.log("\n");

//Severus Results
let severusTotalPoints = 0;
let severusTotalAverage = 0;
let severusAvgPercentage = 0;

severusTotalPoints = severusFirstSubject + severusSecondSubject + severusThirdSubject + severusFourthSubject;
severusTotalAverage = severusTotalPoints / numOfSubjects;
severusAvgPercentage = (severusTotalAverage / maxPoints) * 100;

console.log("%cRESULTS FOR SEVERUS", "color: black; font-size: 19px; font-weight: bold");
console.log("Severus' points total: " + severusTotalPoints);
console.log("Severus' points average: " + severusTotalAverage);
console.log("Severus' average percentage: " + severusAvgPercentage + "%");
console.log("\n");

//Aladdin Results
let aladdinTotalPoints = 0;
let aladdinTotalAverage = 0;
let aladdinAvgPercentage = 0;

aladdinTotalPoints = aladdinFirstSubject + aladdinSecondSubject + aladdinThirdSubject + aladdinFourthSubject;
aladdinTotalAverage = aladdinTotalPoints / numOfSubjects;
aladdinAvgPercentage = (aladdinTotalAverage / maxPoints) * 100;


console.log("%cRESULTS FOR ALADDIN", "color: darkblue; font-size: 19px; font-weight: bold");
console.log("Aladdin's points total: " + aladdinTotalPoints);
console.log("Aladdin's points average: " + aladdinTotalAverage);
console.log("Aladdin's average percentage: " + aladdinAvgPercentage + "%");
console.log("\n");

//Best Student by Points

let bestByPoints = '';

if ((neoTotalPoints > indianaTotalPoints) && (neoTotalPoints > severusTotalPoints) && (neoTotalPoints > aladdinTotalPoints) ) {
    bestByPoints = 'Neo';
}

if ((indianaTotalPoints > neoTotalPoints) && (indianaTotalPoints > severusTotalPoints) && (indianaTotalPoints > aladdinTotalPoints) ) {
    bestByPoints = 'Indiana';
}

if ((severusTotalPoints > neoTotalPoints) && (severusTotalPoints > indianaTotalPoints) && (severusTotalPoints > aladdinTotalPoints) ) {
    bestByPoints = 'Severus';
}

if ((aladdinTotalPoints > neoTotalPoints) && (aladdinTotalPoints > severusTotalPoints) && (aladdinTotalPoints > indianaTotalPoints) ) {
    bestByPoints = 'Aladdin';
}

//აქ ცალკე შეიძლებოდა იმის განხილვა, ემთხვევა თუ არა ვინმეს ქულები ერთმანეთს, იმ შემთხვევაში, თუ მაქსიმალური ქულა არა ერთ, არამედ
//რამდენიმე მოსწავლეს ექნებოდა. მაგრამ ეხლა არ დავწერე, მაგის მარტო if-ებით დაწერა დიდ დროს წაიღებდა.

console.log("\n");
console.log("%cBest Student by Points: " + bestByPoints, "color: tomato; font-size: 22px; font-weight: bold; text-transform: uppercase;");
console.log("\n");


//Credits
const firstSubjectCredit = 4;
const secondSubjectCredit = 2;
const thirdSubjectCredit = 7;
const fourthSubjectCredit = 5;

const sumOfCredits = firstSubjectCredit + secondSubjectCredit + thirdSubjectCredit + fourthSubjectCredit;

console.log("\n");
console.log("%cGPA Results:", "color: green; font-size: 19px; font-weight: bold");

//Neo's GPA
const neoFirstSubjectGPA = 0.5;
const neoSecondSubjectGPA = 0.5;
const neoThirdSubjectGPA = 2;
const neoFourthSubjectGPA = 3;


const neoFirstAvgGPA = neoFirstSubjectGPA * firstSubjectCredit;
const neoSecondAvgGPA = neoSecondSubjectGPA * secondSubjectCredit;
const neoThirdAvgGPA = neoThirdSubjectGPA * thirdSubjectCredit;
const neoFourthAvgGPA = neoFourthSubjectGPA * fourthSubjectCredit;

const neoFinalGPA = (neoFirstAvgGPA + neoSecondAvgGPA + neoThirdAvgGPA + neoFourthAvgGPA) / sumOfCredits;

console.log("Neo's final GPA is: " + neoFinalGPA);

//Indiana's GPA
const indianaFirstSubjectGPA = 2;
const indianaSecondSubjectGPA = 0.5;
const indianaThirdSubjectGPA = 1;
const indianaFourthSubjectGPA = 2;

const indianaFirstAvgGPA = indianaFirstSubjectGPA * firstSubjectCredit;
const indianaSecondAvgGPA = indianaSecondSubjectGPA * secondSubjectCredit;
const indianaThirdAvgGPA = indianaThirdSubjectGPA * thirdSubjectCredit;
const indianaFourthAvgGPA = indianaFourthSubjectGPA * fourthSubjectCredit;

const indianaFinalGPA = (indianaFirstAvgGPA + indianaSecondAvgGPA + indianaThirdAvgGPA + indianaFourthAvgGPA) / sumOfCredits;

console.log("Indiana's final GPA is: " + indianaFinalGPA);

//Severus' GPA
const severusFirstSubjectGPA = 2;
const severusSecondSubjectGPA = 1;
const severusThirdSubjectGPA = 0.5;
const severusFourthSubjectGPA = 3;

const severusFirstAvgGPA = severusFirstSubjectGPA * firstSubjectCredit;
const severusSecondAvgGPA = severusSecondSubjectGPA * secondSubjectCredit;
const severusThirdAvgGPA = severusThirdSubjectGPA * thirdSubjectCredit;
const severusFourthAvgGPA = severusFourthSubjectGPA * fourthSubjectCredit;

const severusFinalGPA = (severusFirstAvgGPA + severusSecondAvgGPA + severusThirdAvgGPA + severusFourthAvgGPA) / sumOfCredits;

console.log("Severus' final GPA is: " + severusFinalGPA);

//Aladdin's GPA
const aladdinFirstSubjectGPA = 2;
const aladdinSecondSubjectGPA = 0.5;
const aladdinThirdSubjectGPA = 1;
const aladdinFourthSubjectGPA = 2;

const aladdinFirstAvgGPA = aladdinFirstSubjectGPA * firstSubjectCredit;
const aladdinSecondAvgGPA = aladdinSecondSubjectGPA * secondSubjectCredit;
const aladdinThirdAvgGPA = aladdinThirdSubjectGPA * thirdSubjectCredit;
const aladdinFourthAvgGPA = aladdinFourthSubjectGPA * fourthSubjectCredit;

const aladdinFinalGPA = (aladdinFirstAvgGPA + aladdinSecondAvgGPA + aladdinThirdAvgGPA + aladdinFourthAvgGPA) / sumOfCredits;

console.log("Aladdin's final GPA is: " + aladdinFinalGPA);